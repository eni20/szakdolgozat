import com.github.javaparser.JavaParser;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitor;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.NotFileFilter;
import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.lang3.StringUtils;

public class LedgerCallChecker {

  public static void main(String[] args) {
    LedgerCallChecker ledgerCallChecker = new LedgerCallChecker();
    ledgerCallChecker.checkLedgerCalls();
  }

  private void checkLedgerCalls() {
    final List<File> files = new ArrayList<>(
            FileUtils.listFiles(new File(Constants.RESOURCES_PATH), new SuffixFileFilter("java"), new NotFileFilter(new
                    PrefixFileFilter(new String[] { "test" }))));
    for (File file : files) {
      CompilationUnit compilationUnit = parseFile(file);
      Map<String, List<String>> unResolvedStubCallsByMethod = processMethods(compilationUnit);
      Map<String, List<String>> stubCallsByMethod = resolveStubCallerMethods(unResolvedStubCallsByMethod);
      if(!stubCallsByMethod.isEmpty()) {
        System.out.println();
        final String filePath = StringUtils.substringAfter(file.getPath(), Constants.RESOURCES_PATH);
        final String filePath2 = StringUtils.substringBefore(filePath, "\\src\\");
        final String filePath3 = StringUtils.substringBefore(filePath2, "\\chaincode-java");
        System.out.println(filePath3 + ": " + file.getName());
        stubCallsByMethod.forEach((method, calls) -> {
          if(!calls.isEmpty()) {
            final String wrongCall = buildWrongStubCallOutputLine(file, method, unResolvedStubCallsByMethod.get(method), stubCallsByMethod);
            System.out.println(wrongCall);
          }
        });
      }

      }
  }

  private CompilationUnit parseFile(File file) {
    try {
      Optional<CompilationUnit> cu = new JavaParser().parse(file).getResult();
      if(cu.isPresent()) {
        return cu.get();
      }
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
    return new CompilationUnit();
  }

  private Map<String, List<String>> processMethods(final CompilationUnit compilationUnit) {
    Set<String> methodNames = new HashSet<>();
    VoidVisitor<Set<String>> methodNameCollector = new MethodNameCollector();
    methodNameCollector.visit(compilationUnit, methodNames);

    Map<String, List<String>> callsByMethod = new HashMap<>();
    compilationUnit.accept(new MethodDeclarationVisitor(methodNames), callsByMethod);

    if (callsByMethod.values().stream().flatMap(List::stream).anyMatch(Constants.LEDGER_CALL_TYPES::contains)) {
      return callsByMethod;
    }
    return new HashMap<>();
  }

  private static class MethodNameCollector extends VoidVisitorAdapter<Set<String>> {

    @Override
    public void visit(MethodDeclaration md, Set<String> collector) {
      super.visit(md, collector);
      collector.add(md.getNameAsString());
    }
  }

  private Map<String, List<String>> resolveStubCallerMethods(
          Map<String, List<String>> originalCallsByMethod) {
    Map<String, List<String>> callsByMethod = new HashMap<>();
    originalCallsByMethod.forEach((method, calls) ->
            callsByMethod.put(method, replaceStubCallerMethod(originalCallsByMethod, calls)));
    if (!originalCallsByMethod.equals(callsByMethod)) {
      return resolveStubCallerMethods(callsByMethod);
    }
    return callsByMethod;
  }

  private List<String> replaceStubCallerMethod(
          Map<String, List<String>> callsByMethod, List<String> calls) {
    if (Constants.LEDGER_CALL_TYPES.containsAll(calls)) {
      return calls;
    }
    List<String> replacedCalls = new ArrayList<>();
    calls.forEach(c -> {
      if (callsByMethod.containsKey(c)) {
        replacedCalls.addAll(callsByMethod.get(c));
      } else if (Arrays.asList(Constants.GET, Constants.POST).contains(c)){
        replacedCalls.add(c);
      }
    });
    return replacedCalls;
  }

  private boolean isStubCallSequenceValid(List<String> calls) {
    boolean previousCallWasGet = false;
    for (String c : calls) {
      if (Constants.GET.equals(c)) {
        previousCallWasGet = true;
      } else if (Constants.POST.equals(c)) {
        if(previousCallWasGet) {
          previousCallWasGet = false;
        } else {
          return false;
        }
      }
    }
    return true;
  }

  private String buildWrongStubCallOutputLine(File file, String method, List<String> unresolvedCalls, Map<String, List<String>> callsBm) {
    final String joinedCalls = StringUtils.join(callsBm.get(method), " ");
    List<String> szurtUnresolved = new ArrayList<>();
    unresolvedCalls.forEach(u -> {
      if(callsBm.containsKey(u) || Constants.LEDGER_CALL_TYPES.contains(u)) {
        szurtUnresolved.add(u);
      }
    });
    final String joinedUCalls = StringUtils.join(szurtUnresolved, " ");
    boolean sequenceValid = isStubCallSequenceValid(callsBm.get(method));
    return StringUtils.joinWith("\t", method, joinedUCalls, joinedCalls, sequenceValid);
  }
}