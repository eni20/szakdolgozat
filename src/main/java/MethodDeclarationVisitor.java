import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.visitor.GenericVisitorAdapter;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import org.apache.commons.lang3.BooleanUtils;

import java.util.*;

public class MethodDeclarationVisitor extends VoidVisitorAdapter<Map<String, List<String>>> {

  private List<String> stubAliases;
  private final Set<String> methodNames;
  private String methodName;

  public MethodDeclarationVisitor(final Set<String> methodNames) {
    this.methodNames = new HashSet<>(methodNames);
  }

  @Override
  public void visit(MethodDeclaration method, Map<String, List<String>> callsByMethod) {
    super.visit(method, callsByMethod);
    if (!method.getBody().isPresent()) {
      return;
    }
    stubAliases = new ArrayList<>();
    stubAliases.add(Constants.GET_STUB_METHOD);
    stubAliases.add("stub");
    methodName = method.getNameAsString();
    final List<String> stubCalls = new ArrayList<>();

    final NodeList<Statement> statements = method.getBody().get().getStatements();
    statements.forEach(s -> s.accept(new StubVariableDeclaratorVisitor(), stubAliases));
    statements.forEach(s -> {
      s.accept(new LambdaMethodCallExprVisitor(), stubCalls);
      s.accept(new MethodCallExprVisitor(), stubCalls);
    });

    if (!stubCalls.isEmpty()) {
      stubCalls.removeAll(Collections.singleton(methodName));
      callsByMethod.put(methodName, stubCalls);
    }
  }

  private class MethodCallExprVisitor extends VoidVisitorAdapter<List<String>> {

    @Override
    public void visit(MethodCallExpr mce, List<String> stubCalls) {
      super.visit(mce, stubCalls);
      final Optional<Expression> scope = mce.getScope();
      if (scope.filter(s ->
              BooleanUtils.isTrue(s.accept(new StubAliasCallVisitor(), stubAliases))).isPresent()
              || !scope.isPresent()) {
        mce.getName().accept(new StubCallerMethodCallVisitor(), stubCalls);
      }
    }
  }

  private class LambdaMethodCallExprVisitor extends VoidVisitorAdapter<List<String>> {

    @Override
    public void visit(LambdaExpr le, List<String> stubCalls) {
      super.visit(le, stubCalls);
      le.getBody().accept(new MethodCallExprVisitor(), stubCalls);
    }
  }

  private class StubAliasCallVisitor extends GenericVisitorAdapter<Boolean, List<String>> {

    @Override
    public Boolean visit(SimpleName sn, List<String> stubAliases) {
      return stubAliases.contains(sn.asString());
    }
  }

  private class StubCallerMethodCallVisitor extends VoidVisitorAdapter<List<String>> {

    @Override
    public void visit(SimpleName ne, List<String> stubCalls) {
      String name = ne.asString();
      if (methodNames.contains(name) && !methodName.equals(name)) {
        stubCalls.add(name);
      } else if (Constants.STUB_GET_METHODS.contains(name)) {
        stubCalls.add(Constants.GET);
      } else if (Constants.STUB_POST_METHODS.contains(name)) {
        stubCalls.add(Constants.POST);
      }
    }
  }
}
