import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class Constants {

  public static final String RESOURCES_PATH = "resources\\";

  public static final String GET_STUB_METHOD = "getStub";
  public static final List<String> STUB_GET_METHODS = Arrays.asList(
      "getPrivateData",
      "getPrivateDataByRange",
      "getPrivateDataHash",
      "getPrivateDataQueryResult",
      "getState",
      "getStateByPartialCompositeKey",
      "getStateByRange",
      "getStringState");

  public static final List<String> STUB_POST_METHODS = Arrays.asList(
      "delPrivateData",
      "delState",
      "putPrivateData",
      "putState",
      "putStringState");

  public static final String GET = "GET";
  public static final String POST = "POST";

  public static final Set<String> LEDGER_CALL_TYPES = new HashSet<>(Arrays.asList(GET, POST));
}