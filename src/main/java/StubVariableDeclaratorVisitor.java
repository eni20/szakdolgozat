import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.visitor.GenericVisitorAdapter;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import java.util.List;
import org.apache.commons.lang3.BooleanUtils;
import org.hyperledger.fabric.shim.ChaincodeStub;

class StubVariableDeclaratorVisitor extends VoidVisitorAdapter<List<String>> {

  @Override
  public void visit(VariableDeclarator vd, List<String> stubAliases) {
    if (vd.getType().isClassOrInterfaceType()
        && vd.getType().asClassOrInterfaceType().getNameAsString().equals(ChaincodeStub.class.getSimpleName())
        && vd.getInitializer().isPresent()
        && BooleanUtils.isTrue(vd.getInitializer().get().accept(new StubCallVisitor(), null))) {
      stubAliases.add(vd.getNameAsString());
    }
  }

  private class StubCallVisitor extends GenericVisitorAdapter<Boolean, Void> {

    @Override
    public Boolean visit(MethodCallExpr mce, Void arg) {
      return mce.getNameAsString().equals(Constants.GET_STUB_METHOD);
    }
  }
}
