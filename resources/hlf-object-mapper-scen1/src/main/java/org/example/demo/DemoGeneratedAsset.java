package org.example.demo;

import java.util.HashMap;
import java.util.logging.Logger;

import org.example.asset.AssetBase;
import org.example.asset.annotation.AssetType;
import org.example.asset.annotation.Attribute;
import org.example.exceptions.ExceptionLogger;
import org.example.exceptions.JsonProcessingFailureException;
import org.example.mapping.impl.SparseCachedContext;
import org.example.utils.LoggerFactory;
import org.hyperledger.fabric.shim.ChaincodeException;

@AssetType(type = "DemoAsset")
public class DemoGeneratedAsset extends AssetBase {

    @Attribute
    public Double pocket1;

    @Attribute
    public Double pocket2;

    @Attribute
    public Double pocket3;

    protected static Logger LOGGER = LoggerFactory.getLogger(DemoGeneratedAsset.class.getName());

    DemoGeneratedAsset() {
        super();
    }

    DemoGeneratedAsset(SparseCachedContext ctx, String uuid) {
        super(ctx, uuid);
    }

    DemoGeneratedAsset(AssetBase nestHost) {
        super(nestHost);
    }

    public Double getPocket1Attribute() {
        try {
            Double res = dataLayer.getAttribute(DemoAsset.class, this, "pocket1", () -> pocket1, null);
            LOGGER.finest("got pocket1 attrib successfully");
            return res;
        } catch (NoSuchFieldException | SecurityException | JsonProcessingFailureException e) {
            ExceptionLogger.log(LOGGER, e);
            throw new ChaincodeException(e);
        }
    }

    public void setPocket1Attribute(Double amount) {
        dataLayer.setAttribute(DemoAsset.class, this, amount, "pocket1", (val) -> this.pocket1 = val);
    }

    public Double getPocket2Attribute() {
        try {
            Double res = dataLayer.getAttribute(DemoAsset.class, this, "pocket2", () -> pocket2, null);
            LOGGER.finest("got pocket2 attrib successfully");
            return res;
        } catch (NoSuchFieldException | SecurityException | JsonProcessingFailureException e) {
            ExceptionLogger.log(LOGGER, e);
            throw new ChaincodeException(e);
        }
    }

    public void setPocket2Attribute(Double amount) {
        dataLayer.setAttribute(DemoAsset.class, this, amount, "pocket2", (val) -> this.pocket2 = val);
    }

    public Double getPocket3Attribute() {
        try {
            Double res = dataLayer.getAttribute(DemoAsset.class, this, "pocket3", () -> pocket3, null);
            LOGGER.finest("got pocket3 attrib successfully");
            return res;
        } catch (NoSuchFieldException | SecurityException | JsonProcessingFailureException e) {
            ExceptionLogger.log(LOGGER, e);
            throw new ChaincodeException(e);
        }
    }

    public void setPocket3Attribute(Double amount) {
        dataLayer.setAttribute(DemoAsset.class, this, amount, "pocket3", (val) -> this.pocket3 = val);
    }

}
