/*
 * SPDX-License-Identifier: Apache-2.0
 */

package org.hyperledger.fabric.samples.privatedata;

import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.Map;
import org.hyperledger.fabric.contract.ClientIdentity;
import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.shim.ChaincodeException;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.CompositeKey;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;
import static org.hyperledger.fabric.samples.privatedata.AssetTransfer.AGREEMENT_KEYPREFIX;
import static org.hyperledger.fabric.samples.privatedata.AssetTransfer.ASSET_COLLECTION_NAME;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public final class AssetTransferTest {

    @Nested
    class InvokeWriteTransaction {

        @Test
        public void createAssetWhenAssetExists() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_properties", DATA_ASSET_1_BYTES);
            when(ctx.getStub().getTransient()).thenReturn(m);
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);

            Throwable thrown = catchThrowable(() -> contract.CreateAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Asset asset1 already exists");
            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("ASSET_ALREADY_EXISTS".getBytes());
        }

        @Test
        public void createAssetWhenNewAssetIsCreated() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getMspId()).thenReturn(TEST_ORG_ONE_MSP);
            ClientIdentity ci = mock(ClientIdentity.class);
            when(ci.getId()).thenReturn(TEST_ORG_1_CLIENT);
            when(ci.getMSPID()).thenReturn(TEST_ORG_ONE_MSP);
            when(ctx.getClientIdentity()).thenReturn(ci);

            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_properties", DATA_ASSET_1_BYTES);
            when(ctx.getStub().getTransient()).thenReturn(m);

            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(new byte[0]);

            Asset created = contract.CreateAsset(ctx);
            assertThat(created).isEqualTo(TEST_ASSET_1);

            verify(stub).putPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID, created.serialize());
        }

        @Test
        public void transferAssetWhenExistingAssetIsTransferred() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getMspId()).thenReturn(TEST_ORG_ONE_MSP);
            ClientIdentity ci = mock(ClientIdentity.class);
            when(ci.getId()).thenReturn(TEST_ORG_1_CLIENT);
            when(ctx.getClientIdentity()).thenReturn(ci);
            when(ci.getMSPID()).thenReturn(TEST_ORG_ONE_MSP);
            final String recipientOrgMsp = "TestOrg2";
            final String buyerIdentity = "TestOrg2User";
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_owner", ("{ \"buyerMSP\": \"" + recipientOrgMsp + "\", \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            when(stub.getPrivateDataHash(anyString(), anyString())).thenReturn("TestHashValue".getBytes());
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);
            CompositeKey ck = mock(CompositeKey.class);
            when(ck.toString()).thenReturn(AGREEMENT_KEYPREFIX + TEST_ASSET_1_ID);
            when(stub.createCompositeKey(AGREEMENT_KEYPREFIX, TEST_ASSET_1_ID)).thenReturn(ck);
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, AGREEMENT_KEYPREFIX + TEST_ASSET_1_ID)).thenReturn(buyerIdentity.getBytes(UTF_8));
            contract.TransferAsset(ctx);

            Asset exptectedAfterTransfer  = Asset.deserialize("{ \"objectType\": \"testasset\", \"assetID\": \"asset1\", \"color\": \"blue\", \"size\": 5, \"owner\": \"" +  buyerIdentity + "\", \"appraisedValue\": 300 }");

            verify(stub).putPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID, exptectedAfterTransfer.serialize());
            String collectionOwner = TEST_ORG_ONE_MSP + "PrivateCollection";
            verify(stub).delPrivateData(collectionOwner, TEST_ASSET_1_ID);
            verify(stub).delPrivateData(ASSET_COLLECTION_NAME, AGREEMENT_KEYPREFIX + TEST_ASSET_1_ID);
        }
    }

    @Nested
    class QueryReadAssetTransaction {

        @Test
        public void whenAssetExists() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);

            Asset asset = contract.ReadAsset(ctx, TEST_ASSET_1_ID);

            assertThat(asset).isEqualTo(TEST_ASSET_1);
        }

        @Test
        public void whenAssetDoesNotExist() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getStringState(TEST_ASSET_1_ID)).thenReturn(null);

            Asset asset = contract.ReadAsset(ctx, TEST_ASSET_1_ID);
            assertThat(asset).isNull();
        }

        @Test
        public void invokeUnknownTransaction() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);

            Throwable thrown = catchThrowable(() -> contract.unknownTransaction(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Undefined contract method called");
            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo(null);

            verifyZeroInteractions(ctx);
        }

    }

    @Nested
    class QueryReadAssetPrivateDetailsTransaction {

        @Test
        public void whenAssetExists() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);

            AssetPrivateDetails asset = contract.ReadAssetPrivateDetails(ctx, ASSET_COLLECTION_NAME, TEST_ASSET_1_ID);

            assert asset != null;
            assertThat(asset.getAssetID()).isEqualTo(AssetPrivateDetails.deserialize(DATA_ASSET_1_BYTES).getAssetID());
            assertThat(asset.getAppraisedValue()).isEqualTo(AssetPrivateDetails.deserialize(DATA_ASSET_1_BYTES).getAppraisedValue());
        }

        @Test
        public void whenAssetDoesNotExist() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getStringState(TEST_ASSET_1_ID)).thenReturn(null);

            AssetPrivateDetails asset = contract.ReadAssetPrivateDetails(ctx, ASSET_COLLECTION_NAME, TEST_ASSET_1_ID);
            assertThat(asset).isNull();
        }
    }

    @Nested
    class CreateAssetNegativeTest {

        @Test
        public void createAssetNoTransientMap() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);

            Throwable thrown = catchThrowable(() -> contract.CreateAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("CreateAsset call must specify asset_properties in Transient map input");
        }

        @Test
        public void createAssetWrongJson() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_properties", "asd".getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);

            Throwable thrown = catchThrowable(() -> contract.CreateAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessageStartingWith("TransientMap deserialized error:");
            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("INCOMPLETE_INPUT".getBytes());
        }

        @Test
        public void createAssetEmptyInputs() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_properties", WRONG_ASSET_BYTES);
            when(ctx.getStub().getTransient()).thenReturn(m);
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);

            Throwable thrown = catchThrowable(() -> contract.CreateAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Empty input in Transient map: appraisedValue");
            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("INCOMPLETE_INPUT".getBytes());
        }

        @Test
        public void createAssetVerifyOrgNotEquals() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getMspId()).thenReturn("TestOrg2");
            ClientIdentity ci = mock(ClientIdentity.class);
            when(ci.getMSPID()).thenReturn(TEST_ORG_ONE_MSP);
            when(ctx.getClientIdentity()).thenReturn(ci);

            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_properties", DATA_ASSET_1_BYTES);
            when(ctx.getStub().getTransient()).thenReturn(m);

            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(new byte[0]);

            Throwable thrown = catchThrowable(() -> contract.CreateAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessageContaining("is not authorized to read or write private data from an org ");
            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("INVALID_ACCESS".getBytes());
        }
    }

    @Nested
    class TransferAssetNegativeTest {

        @Test
        public void transferAssetNoTransientMap() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Throwable thrown = catchThrowable(() -> contract.TransferAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("TransferAsset call must specify \"asset_owner\" in Transient map input");
        }

        @Test
        public void transferAssetWrongJson() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_owner", "asd".getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            Throwable thrown = catchThrowable(() -> contract.TransferAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessageStartingWith("TransientMap deserialized error");
            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("INCOMPLETE_INPUT".getBytes());
        }

        @Test
        public void transferAssetEmptyAssetId() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_owner", ("{ \"buyerMSP\": \"\", \"assetID\": \"\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            Throwable thrown = catchThrowable(() -> contract.TransferAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Invalid input in Transient map: assetID");
            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("INCOMPLETE_INPUT".getBytes());
        }

        @Test
        public void transferAssetBuyerMsp() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_owner", ("{ \"buyerMSP\": \"\", \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            Throwable thrown = catchThrowable(() -> contract.TransferAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Invalid input in Transient map: buyerMSP");
            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("INCOMPLETE_INPUT".getBytes());
        }

        @Test
        public void transferAssetNoExist() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            final String recipientOrgMsp = "TestOrg2";
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_owner", ("{ \"buyerMSP\": \"" + recipientOrgMsp + "\", \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            Throwable thrown = catchThrowable(() -> contract.TransferAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessageEndingWith("does not exist in the collection");
            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("INCOMPLETE_INPUT".getBytes());
        }

        @Test
        public void transferAssetNoTransferAgreement() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getMspId()).thenReturn(TEST_ORG_ONE_MSP);
            ClientIdentity ci = mock(ClientIdentity.class);
            when(ci.getId()).thenReturn(TEST_ORG_1_CLIENT);
            when(ctx.getClientIdentity()).thenReturn(ci);
            when(ci.getMSPID()).thenReturn(TEST_ORG_ONE_MSP);
            final String recipientOrgMsp = "TestOrg2";
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_owner", ("{ \"buyerMSP\": \"" + recipientOrgMsp + "\", \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            when(stub.getPrivateDataHash(anyString(), anyString())).thenReturn("TestHashValue".getBytes());
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);
            CompositeKey ck = mock(CompositeKey.class);
            when(stub.createCompositeKey(AGREEMENT_KEYPREFIX, TEST_ASSET_1_ID)).thenReturn(ck);

            Throwable thrown = catchThrowable(() -> contract.TransferAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessageStartingWith("TransferAgreement does not exist for asset:");
            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("INCOMPLETE_INPUT".getBytes());
        }

        @Test
        public void verifyAgreementClientIdNotOwn() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getMspId()).thenReturn(TEST_ORG_ONE_MSP);
            ClientIdentity ci = mock(ClientIdentity.class);
            when(ci.getId()).thenReturn("testOrg2User");
            when(ctx.getClientIdentity()).thenReturn(ci);
            when(ci.getMSPID()).thenReturn(TEST_ORG_ONE_MSP);
            final String recipientOrgMsp = "TestOrg2";
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_owner", ("{ \"buyerMSP\": \"" + recipientOrgMsp + "\", \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            when(stub.getPrivateDataHash(anyString(), anyString())).thenReturn("TestHashValue".getBytes());
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);

            Throwable thrown = catchThrowable(() -> contract.TransferAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessageStartingWith("Submitting client identity does not own the asset");
            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("INVALID_ACCESS".getBytes());

        }

        @Test
        public void verifyAgreementOwnerHashNotExist() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getMspId()).thenReturn(TEST_ORG_ONE_MSP);
            ClientIdentity ci = mock(ClientIdentity.class);
            when(ci.getId()).thenReturn(TEST_ORG_1_CLIENT);
            when(ctx.getClientIdentity()).thenReturn(ci);
            when(ci.getMSPID()).thenReturn(TEST_ORG_ONE_MSP);
            final String recipientOrgMsp = "TestOrg2";
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_owner", ("{ \"buyerMSP\": \"" + recipientOrgMsp + "\", \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            when(stub.getPrivateDataHash(anyString(), anyString())).thenReturn(null);
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);

            Throwable thrown = catchThrowable(() -> contract.TransferAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Hash of appraised value for asset1 does not exist in collection TestOrg1PrivateCollection");
        }

        @Test
        public void verifyAgreementBuyerHashNotExist() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getMspId()).thenReturn(TEST_ORG_ONE_MSP);
            ClientIdentity ci = mock(ClientIdentity.class);
            when(ci.getId()).thenReturn(TEST_ORG_1_CLIENT);
            when(ctx.getClientIdentity()).thenReturn(ci);
            when(ci.getMSPID()).thenReturn(TEST_ORG_ONE_MSP);
            final String recipientOrgMsp = "TestOrg2";
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_owner", ("{ \"buyerMSP\": \"" + recipientOrgMsp + "\", \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            when(stub.getPrivateDataHash(TEST_ORG_ONE_MSP + "PrivateCollection", TEST_ASSET_1_ID)).thenReturn("TestHashValue".getBytes());
            when(stub.getPrivateDataHash(recipientOrgMsp + "PrivateCollection", TEST_ASSET_1_ID)).thenReturn(null);
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);

            Throwable thrown = catchThrowable(() -> contract.TransferAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Hash of appraised value for asset1 does not exist in collection TestOrg2PrivateCollection. AgreeToTransfer must be called by the buyer first.");
        }

        @Test
        public void verifyAgreementOwnerBuyerHashNotEquals() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getMspId()).thenReturn(TEST_ORG_ONE_MSP);
            ClientIdentity ci = mock(ClientIdentity.class);
            when(ci.getId()).thenReturn(TEST_ORG_1_CLIENT);
            when(ctx.getClientIdentity()).thenReturn(ci);
            when(ci.getMSPID()).thenReturn(TEST_ORG_ONE_MSP);
            final String recipientOrgMsp = "TestOrg2";
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_owner", ("{ \"buyerMSP\": \"" + recipientOrgMsp + "\", \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            when(stub.getPrivateDataHash(TEST_ORG_ONE_MSP + "PrivateCollection", TEST_ASSET_1_ID)).thenReturn("TestHashValue1".getBytes());
            when(stub.getPrivateDataHash(recipientOrgMsp + "PrivateCollection", TEST_ASSET_1_ID)).thenReturn("TestHashValue2".getBytes());
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);

            Throwable thrown = catchThrowable(() -> contract.TransferAsset(ctx));

            assertThat(thrown).isInstanceOf(IllegalFormatException.class).hasNoCause();
        }
    }

    @Nested
    class DeleteAssetTransaction {

        @Test
        public void deleteAssetWhenAssetExists() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getMspId()).thenReturn(TEST_ORG_ONE_MSP);
            ClientIdentity ci = mock(ClientIdentity.class);
            when(ci.getId()).thenReturn(TEST_ORG_1_CLIENT);
            when(ctx.getClientIdentity()).thenReturn(ci);
            when(ci.getMSPID()).thenReturn(TEST_ORG_ONE_MSP);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_delete", ("{ \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);
            String collectionOwner = TEST_ORG_ONE_MSP + "PrivateCollection";
            when(stub.getPrivateData(collectionOwner, TEST_ASSET_1_ID)).thenReturn(DATA_ASSET_1_BYTES);

            contract.DeleteAsset(ctx);

            verify(stub).delPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID);
            verify(stub).delPrivateData(collectionOwner, TEST_ASSET_1_ID);
        }

        @Test
        public void deleteAssetNoTransientMap() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);

            Throwable thrown = catchThrowable(() -> contract.DeleteAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("DeleteAsset call must specify 'asset_delete' in Transient map input");
        }

        @Test
        public void deleteAssetWrongJson() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_delete", "asd".getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            Throwable thrown = catchThrowable(() -> contract.DeleteAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("TransientMap deserialized error: org.json.JSONException: A JSONObject text must begin with '{' at 1 [character 2 line 1] ");
        }

        @Test
        public void deleteAssetWhenAssetNotExists() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_delete", ("{ \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            Throwable thrown = catchThrowable(() -> contract.DeleteAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Asset asset1 does not exist");
        }

        @Test
        public void deleteAssetWhenAssetNotExistsInOwnersCollection() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            ClientIdentity ci = mock(ClientIdentity.class);
            when(ci.getId()).thenReturn(TEST_ORG_1_CLIENT);
            when(ctx.getClientIdentity()).thenReturn(ci);
            when(ci.getMSPID()).thenReturn(TEST_ORG_ONE_MSP);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_delete", ("{ \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);

            Throwable thrown = catchThrowable(() -> contract.DeleteAsset(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Failed to read asset from owner's Collection TestOrg1PrivateCollection");
        }
    }

    @Nested
    class AgreeToTransferTransaction {

        @Test
        public void agreeToTransferWhenAssetExist() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getMspId()).thenReturn(TEST_ORG_ONE_MSP);
            ClientIdentity ci = mock(ClientIdentity.class);
            when(ci.getId()).thenReturn(TEST_ORG_1_CLIENT);
            when(ctx.getClientIdentity()).thenReturn(ci);
            when(ci.getMSPID()).thenReturn(TEST_ORG_ONE_MSP);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_value", ("{ \"appraisedValue\": \"300\", \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);
            when(stub.getPrivateData(ASSET_COLLECTION_NAME, TEST_ASSET_1_ID))
                    .thenReturn(DATA_ASSET_1_BYTES);
            CompositeKey ck = mock(CompositeKey.class);
            when(ck.toString()).thenReturn(AGREEMENT_KEYPREFIX + TEST_ASSET_1_ID);
            when(stub.createCompositeKey(AGREEMENT_KEYPREFIX, TEST_ASSET_1_ID)).thenReturn(ck);
            contract.AgreeToTransfer(ctx);

            AssetPrivateDetails assetPriv  = AssetPrivateDetails.deserialize(m.get("asset_value"));

            String collectionOwner = TEST_ORG_ONE_MSP + "PrivateCollection";
            verify(stub).putPrivateData(collectionOwner, TEST_ASSET_1_ID, assetPriv.serialize());
            verify(stub).putPrivateData(ASSET_COLLECTION_NAME, AGREEMENT_KEYPREFIX + TEST_ASSET_1_ID, TEST_ORG_1_CLIENT);
        }

        @Test
        public void agreeToTransferNoTransientMap() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);

            Throwable thrown = catchThrowable(() -> contract.AgreeToTransfer(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("AgreeToTransfer call must specify \"asset_value\" in Transient map input");
        }

        @Test
        public void agreeToTransferWrongJson() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_value", "asd".getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            Throwable thrown = catchThrowable(() -> contract.AgreeToTransfer(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("TransientMap deserialized error org.json.JSONException: A JSONObject text must begin with '{' at 1 [character 2 line 1] ");
        }

        @Test
        public void agreeToTransferNoAssetID() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_value", WRONG_ASSET_BYTES);
            when(ctx.getStub().getTransient()).thenReturn(m);

            Throwable thrown = catchThrowable(() -> contract.AgreeToTransfer(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Invalid input in Transient map: assetID");
        }

        @Test
        public void agreeToTransferWrongAppraisedValue() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_value", "{ \"assetID\": \"asset1\", \"appraisedValue\": -1 }".getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            Throwable thrown = catchThrowable(() -> contract.AgreeToTransfer(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Input must be positive integer: appraisedValue");
        }

        @Test
        public void agreeToTransferWhenAssetNotExist() {
            AssetTransfer contract = new AssetTransfer();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            Map<String, byte[]> m = new HashMap<>();
            m.put("asset_value", ("{ \"appraisedValue\": \"300\", \"assetID\": \"" + TEST_ASSET_1_ID + "\" }").getBytes());
            when(ctx.getStub().getTransient()).thenReturn(m);

            Throwable thrown = catchThrowable(() -> contract.AgreeToTransfer(ctx));

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Asset does not exist in the collection: ");
        }
    }

    private static final String TEST_ORG_ONE_MSP = "TestOrg1";
    private static final String TEST_ORG_1_CLIENT = "testOrg1User";

    private static final String TEST_ASSET_1_ID = "asset1";
    private static final Asset TEST_ASSET_1 = new Asset("testasset", "asset1", "blue", 5, TEST_ORG_1_CLIENT);
    private static final byte[] DATA_ASSET_1_BYTES = "{ \"objectType\": \"testasset\", \"assetID\": \"asset1\", \"color\": \"blue\", \"size\": 5, \"owner\": \"testOrg1User\", \"appraisedValue\": 300 }".getBytes();
    private static final byte[] WRONG_ASSET_BYTES = "{ \"objectType\": \"\", \"assetID\": \"\", \"color\": \"\", \"size\": -1, \"owner\": \"\", \"appraisedValue\": -1 }".getBytes();
}
